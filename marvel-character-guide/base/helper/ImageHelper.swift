//
//  ImageHelper.swift
//  marvel-character-guide
//
//  Created by Luis Emilio Dias Wolf on 22/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import UIKit

class ImageHelper {
    
    static let sharedInstance = ImageHelper()
    private init() {}
    
    fileprivate var requestHelper = RequestHelper()
    
    func getImage(withPath path: String, withCompletion completion: @escaping (UIImage?)->Void) {
        if let image = CacheHelper.sharedInstance.getImage(forKey: path) {
            completion(image)
        } else {
            requestHelper.download(fromUrl: path) { data in
                if let data = data, let image = UIImage(data: data) {
                    CacheHelper.sharedInstance.storeImage(image, forKey: path)
                    completion(image)
                } else {
                    completion(nil)
                }
            }
        }
    }
    
}
