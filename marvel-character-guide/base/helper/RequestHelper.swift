//
//  RequestHelper.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 21/12/19.
//

import Foundation
import Alamofire

enum RequestError: Error {
    case urlError, statusError, parseError
}

final class RequestHelper {
    
    private let imageTypes = ["image/png", "image/jpeg"]
    
    fileprivate enum Params {
        static let apikey = "apikey"
        static let hash = "hash"
        static let timestamp = "ts"
    }
    
    func getObject<T>(ofType: T.Type, fromUrl url: String, andReturnTo callback: @escaping (_ : () throws -> DataWrapper<DataContainer<T>>) -> Void) where T: Decodable {
    
        makeRequest(fromUrl: url) { response in
            do {
                let data = try response()
                do {
                    let obj = try JSONDecoder().decode(DataWrapper<DataContainer<T>>.self, from: data)
                    callback({ return obj })
                } catch {
                    callback({ throw RequestError.parseError })
                }
            } catch let error {
                callback({ throw error })
            }
        }
        
    }
    
    func getArray<T>(ofType: T.Type, fromUrl url: String, andReturnTo callback: @escaping (_ : () throws -> DataWrapper<DataContainer<[T]>>) -> Void) where T: Decodable {
        
        makeRequest(fromUrl: url) { response in
            do {
                let data = try response()
                do {
                    let obj = try JSONDecoder().decode(DataWrapper<DataContainer<[T]>>.self, from: data)
                    callback({ return obj })
                } catch {
                    callback({ throw RequestError.parseError })
                }
            } catch let error {
                callback({ throw error })
            }
        }
        
    }
    
    func download(fromUrl url: String, andReturnTo callback: @escaping (Data?) -> Void) {
        
        Alamofire.request(url)
            .validate(contentType: self.imageTypes)
            .responseData { response in
                DispatchQueue.main.async {
                    callback(response.data)
                }
        }
        
    }
    
}

extension RequestHelper {
    
    fileprivate func makeRequest(fromUrl url: String, andReturnTo callback: @escaping (_ : () throws -> Data) -> Void) {
        guard let urlString = getUrl(fromUrlString: url) else {
            callback({ throw RequestError.urlError })
            return
        }
        
        Alamofire.request(urlString).responseData { response in
            guard case let .success(data) = response.result else {
                DispatchQueue.main.async {
                    callback({ throw RequestError.statusError })
                }
                return
            }
            DispatchQueue.main.async {
                callback({ return data })
            }
        }
    }
    
    fileprivate func getUrl(fromUrlString urlString: String) -> String?  {
        
        let timestamp = "\(Int64(Date().timeIntervalSince1970 * 1000.0))"
        let hash = timestamp.appending(AppConstant.Api.privateKey).appending(AppConstant.Api.publicKey).md5Value
        
        var url = URLComponents(string: urlString)
        if url?.queryItems == nil {
            url?.queryItems = [URLQueryItem]()
        }
        url?.queryItems?.append(URLQueryItem(name: Params.apikey, value: AppConstant.Api.publicKey))
        url?.queryItems?.append(URLQueryItem(name: Params.timestamp, value: timestamp))
        url?.queryItems?.append(URLQueryItem(name: Params.hash, value: hash))
        
        return url?.string
    }
    
}
