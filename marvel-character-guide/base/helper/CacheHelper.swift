//
//  CacheHelper.swift
//  marvel-character-guide
//
//  Created by Luis Emilio Dias Wolf on 22/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import UIKit

class CacheHelper {
    private let image = NSCache<NSString, UIImage>()
    
    static let sharedInstance = CacheHelper()
    private init() {}
}

// MARK: - Image
extension CacheHelper {
    func getImage(forKey key: String) -> UIImage? {
        return image.object(forKey: NSString(string: key))
    }
    func storeImage(_ image: UIImage, forKey key: String) {
        guard getImage(forKey: key) == nil else { return }
        self.image.setObject(image, forKey: NSString(string: key))
    }
    func removeImage(forKey key: String) {
        self.image.removeObject(forKey: NSString(string: key))
    }
}
