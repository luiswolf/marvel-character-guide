//
//  AppDelegate.swift
//  marvel-character-guide
//
//  Created by Luis Emilio Dias Wolf on 21/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        AppDelegate.configureMainAppearance()
        let characters = CharactersTableViewController.instantiate(navigationControllerIn: CharactersConstant.Storyboard.main)

        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = characters
        window?.makeKeyAndVisible()
        
        return true
    }

}

// MARK: - Helper
extension AppDelegate {
    
    fileprivate static func configureMainAppearance() {
        UINavigationBar.appearance().barTintColor = UIColor.lightBlack.withAlphaComponent(0.5)
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [
            .foregroundColor : UIColor.white
        ]
        UIButton.appearance().tintColor = UIColor.orange
        UITableView.appearance().separatorColor = UIColor.darkGray
        UITableViewCell.appearance().backgroundColor = UIColor.gray
    }
    
}
