//
//  DataWrapper.swift
//  marvel-character-guide
//
//  Created by Luis Emilio Dias Wolf on 21/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation

struct DataWrapper<T: DataContainerProtocol & Decodable>: DataWrapperProtocol, Decodable {
    
    typealias DataContainerType = T
    
    var code: Int?
    var status: String?
    var data: T?
    
}
