//
//  DataContainer.swift
//  marvel-character-guide
//
//  Created by Luis Emilio Dias Wolf on 21/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation

struct DataContainer<T: Decodable>: DataContainerProtocol, Decodable {
    
    var offset: Int?
    var limit: Int?
    var total: Int?
    var count: Int?
    var results: [T]?
    
}
