//
//  Image.swift
//  marvel-character-guide
//
//  Created by Luis Emilio Dias Wolf on 22/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation

struct Image: Decodable {
    
    var path: String?
    var ext: String?
    
    enum ImageKeys: String, CodingKey {
        case path = "path"
        case ext = "extension"
    }
    
    init(path: String?, ext: String?) {
        self.path = path
        self.ext = ext
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ImageKeys.self)
        let path = try container.decode(String.self, forKey: .path)
        let ext = try container.decode(String.self, forKey: .ext)
        self.init(path: path, ext: ext)
    }
    
}

// MARK: - Helper
extension Image {
    
    func getFullPath() -> String? {
        guard let path = path, let ext = ext else { return nil }
        return path.appending(".").appending(ext)
    }
    
}
