//
//  UIColor.swift
//  marvel-character-guide
//
//  Created by Luis Emilio Dias Wolf on 22/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import UIKit

extension UIColor {
    static let red = #colorLiteral(red: 0.8862745098, green: 0.2117647059, blue: 0.2117647059, alpha: 1)
    static let darkRed = #colorLiteral(red: 0.4, green: 0.007843137255, blue: 0.007843137255, alpha: 1)
    static let lightBlack = #colorLiteral(red: 0.1411764706, green: 0.1411764706, blue: 0.1411764706, alpha: 1)
    static let black = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    static let lightGray = #colorLiteral(red: 0.4278482795, green: 0.4278482795, blue: 0.4278482795, alpha: 1)
    static let gray = #colorLiteral(red: 0.3179988265, green: 0.3179988265, blue: 0.3179988265, alpha: 1)
    static let darkGray = #colorLiteral(red: 0.1882352941, green: 0.1882352941, blue: 0.1882352941, alpha: 1)
    static let blue = #colorLiteral(red: 0.3176470588, green: 0.5490196078, blue: 0.7921568627, alpha: 1)
    static let orange = #colorLiteral(red: 0.968627451, green: 0.5607843137, blue: 0.2470588235, alpha: 1)
    static let white = #colorLiteral(red: 0.9202682376, green: 0.9202682376, blue: 0.9202682376, alpha: 1)
}
