//
//  UIFont.swift
//  marvel-character-guide
//
//  Created by Luis Emilio Dias Wolf on 22/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import UIKit

extension UIFont {
    static let regular = UIFont(name: "SFProText-Regular", size: 14.0)!
    static let bold = UIFont(name: "SFProText-Semibold", size: 14.0)!
    static let display = UIFont(name: "SFProDisplay-Regular", size: 14.0)!
}
