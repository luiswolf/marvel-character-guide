//
//  ViewController.swift
//  marvel-character-guide
//
//  Created by Luis Emilio Dias Wolf on 22/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import UIKit

class ViewController: UIViewController, ViewControllerProtocol {
    
    lazy var activityIndicator = UIActivityIndicatorView()
    
    lazy var loaderView: UIView = {
        let loader = UIView()
        loader.layer.zPosition = 998
        loader.frame = view.bounds
        loader.translatesAutoresizingMaskIntoConstraints = false
        loader.backgroundColor = UIColor.lightBlack
        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        activityIndicator.style = .whiteLarge
        activityIndicator.color = .gray
        activityIndicator.hidesWhenStopped = true
        activityIndicator.center = loader.center
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        loader.addSubview(activityIndicator)
        activityIndicator.centerXAnchor.constraint(equalTo: loader.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: loader.centerYAnchor).isActive = true
        activityIndicator.startAnimating()
        return loader
    }()
    
    lazy var infoView: InfoView = {
        let infoView = InfoView()
        infoView.frame = view.bounds
        infoView.layer.zPosition = 999
        infoView.alpha = 0.0
        infoView.translatesAutoresizingMaskIntoConstraints = false
        return infoView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isTranslucent = false
    }
}
