//
//  DisclosureIndicator.swift
//  marvel-character-guide
//
//  Created by Luis Emilio Dias Wolf on 22/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import UIKit

class DisclosureIndicator: UIView {
    
    var color = UIColor.darkGray {
        didSet {
            setNeedsDisplay()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    convenience init() {
        self.init(frame: CGRect(x: 0, y: 0, width: 8, height: 16))
        backgroundColor = UIColor.clear
    }
    convenience init(withColor color: UIColor) {
        self.init()
        self.color = color
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override func draw(_ rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        
        let x = bounds.maxX - 1
        let y = bounds.midY
        let R = CGFloat(5.4)
        context?.move(to: CGPoint(x: x - R, y: y - R))
        context?.addLine(to: CGPoint(x: x, y: y))
        context?.addLine(to: CGPoint(x: x - R, y: y + R))
        context?.setLineCap(.square)
        context?.setLineJoin(.miter)
        context?.setLineWidth(1.7)
        color.setStroke()
        context!.strokePath()
    }
}
