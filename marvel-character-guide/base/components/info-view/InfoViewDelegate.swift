//
//  InfoViewDelegate.swift
//  marvel-character-guide
//
//  Created by Luis Emilio Dias Wolf on 22/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation

protocol InfoViewDelegate: class {
    
    func didPressRetryButton()
    
}
