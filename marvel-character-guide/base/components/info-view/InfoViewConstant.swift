//
//  InfoViewConstant.swift
//  marvel-character-guide
//
//  Created by Luis Emilio Dias Wolf on 22/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation

/// Constants for the Info View component
enum InfoViewConstant {
    
    /// List of titles used in Info package
    enum Title {
        static let noData = "Ops"
        static let notice = "Aviso"
        static let noConnection = "Sem conexão"
    }
    
    /// List of images used in Info package
    enum Image {
        static let noData = "empty"
        static let error = "error"
    }
    
    /// List of messages used in Info package
    enum Message {
        static let error = "Ocorreu um erro inesperado. Por favor, tente novamente mais tarde."
        static let noConnection = "Você está offline. Por favor verifique sua conexão com a internet."
    }
    
}
