//
//  InfoView.swift
//  marvel-character-guide
//
//  Created by Luis Emilio Dias Wolf on 22/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import UIKit



class InfoView: UIView {
    
    enum State {
        case noConnection, error, noData
    }
    
    weak var delegate: InfoViewDelegate?
    
    @IBOutlet fileprivate weak var iconImageView: UIImageView!
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    @IBOutlet fileprivate weak var messageLabel: UILabel!
    @IBOutlet fileprivate weak var actionButton: UIButton! {
        didSet {
//            actionButton?.titleLabel?.font = UIFont.regular.withSize(16.0)
        }
    }
    
    var state: State = .error {
        didSet {
            switch state {
            case .error:
                iconImageView?.image = UIImage(named: InfoViewConstant.Image.error)
                titleLabel?.text = InfoViewConstant.Title.notice
            case .noConnection:
                iconImageView?.image = UIImage(named: InfoViewConstant.Image.error)
                titleLabel?.text = InfoViewConstant.Title.noConnection
                message = InfoViewConstant.Message.noConnection
            case .noData:
                iconImageView?.image = UIImage(named: InfoViewConstant.Image.noData)
                titleLabel?.text = InfoViewConstant.Title.noData
            }
        }
    }
    var message: String! = InfoViewConstant.Message.error {
        didSet {
            messageLabel?.text = message
        }
    }
    var title: String! = InfoViewConstant.Title.notice {
        didSet {
            titleLabel?.text = title
        }
    }
    var hasActionButton: Bool = true {
        didSet {
            actionButton?.isHidden = !hasActionButton
        }
    }
    
    private var contentView : UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        xibSetup()
        contentView?.prepareForInterfaceBuilder()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func xibSetup() {
        contentView = loadViewFromNib()
        contentView.frame = bounds
        contentView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        addSubview(contentView)
    }
    func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
}

extension InfoView {
    @IBAction
    final fileprivate func didPressRetryButton(_ sender: UIButton) {
        delegate?.didPressRetryButton()
    }
}

