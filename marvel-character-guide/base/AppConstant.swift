//
//  AppConstant.swift
//  marvel-character-guide
//
//  Created by Luis Emilio Dias Wolf on 22/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation

enum AppConstant {
    
    enum Api {
        static let baseURL = "https://gateway.marvel.com/v1/public/"
        static let publicKey = "dba7fdcad42b1128b1480709445501e9"
        static let privateKey = "d9de1a67f70e355065035766e8a379bed820123e"
    }
    
    enum Image {
        static let logo = "marvel"
        static let background = "background"
        static let backgroundCell = "background-cell"
    }
    
    enum Message {
        static let unknownError = "Ocorreu um erro inesperado."
        static let networkError = "Não foi possível buscar dados. Tente novamente mais tarde."
        static let loadingNextPage = "Carregando página %d de %d..."
        static let loading = "Carregando..."
    }
    
}
