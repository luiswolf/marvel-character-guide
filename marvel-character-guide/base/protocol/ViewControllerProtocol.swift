//
//  ViewControllerProtocol.swift
//  marvel-character-guide
//
//  Created by Luis Emilio Dias Wolf on 22/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import UIKit

protocol ViewControllerProtocol  {
    func startLoading()
    func stopLoading()
}

extension ViewControllerProtocol where Self: ViewController {
    func add(child viewController: UIViewController?, toContainer container: UIView) {
        guard let viewController = viewController else { return }
        addChild(viewController)
        container.addSubview(viewController.view)
        viewController.view.frame = container.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParent: self)
    }
    func remove(child viewController: UIViewController?) {
        guard let viewController = viewController else { return }
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
    func startLoading() {
        view.addSubview(loaderView)
        setConstraints(forView: loaderView)
    }
    func stopLoading() {
        loaderView.removeFromSuperview()
    }
    func showInfo() {
        infoView.alpha = 0.0
        view.addSubview(infoView)
        setConstraints(forView: infoView)
        infoView.alpha = 1.0
    }
    func hideInfo() {
        infoView.alpha = 0.0
        infoView.removeFromSuperview()
    }
    private func setConstraints(forView v: UIView) {
        if #available(iOS 11.0, *) {
            v.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0.0).isActive = true
            v.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: 0.0).isActive = true
            v.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: 0).isActive = true
            v.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        } else {
            v.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: 0.0).isActive = true
            v.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0.0).isActive = true
            v.widthAnchor.constraint(equalTo: view.widthAnchor, constant: 0.0).isActive = true
            v.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor, constant: 0).isActive = true
        }
    }
}

extension ViewControllerProtocol where Self: TableViewController {
    func startLoading() {
        tableView.setContentOffset(.zero, animated: false)
        tableView.isScrollEnabled = false
        isLoaderVisible = true
        if backgroundColor != nil {
            view.backgroundColor = UIColor.lightBlack
        }
        view.addSubview(loaderView)
        setConstraints(forView: loaderView)
    }
    func stopLoading() {
        if backgroundColor != nil {
            view.backgroundColor = backgroundColor
        }
        tableView.isScrollEnabled = true
        isLoaderVisible = false
        loaderView.removeFromSuperview()
    }
    func showInfo(animated: Bool = false) {
        tableView.setContentOffset(.zero, animated: false)
        tableView.isScrollEnabled = false
        if backgroundColor != nil {
            view.backgroundColor = UIColor.lightBlack
        }
        isInfoVisible = true
        view.addSubview(infoView)
        setConstraints(forView: infoView)
        self.infoView.alpha = 1.0
    }
    func hideInfo(animated: Bool = false) {
        if !isLoaderVisible {
            if backgroundColor != nil {
                view.backgroundColor = backgroundColor
            }
            tableView.isScrollEnabled = true
        }
        infoView.alpha = 0.0
        isInfoVisible = false
        infoView.removeFromSuperview()
    }
    private func setConstraints(forView v: UIView) {
        if #available(iOS 11.0, *) {
            v.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0.0).isActive = true
            v.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: 0.0).isActive = true
            v.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: 0).isActive = true
            v.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        } else {
            v.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: 0.0).isActive = true
            v.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0.0).isActive = true
            v.widthAnchor.constraint(equalTo: view.widthAnchor, constant: 0.0).isActive = true
            v.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor, constant: 0).isActive = true
        }
    }
}
