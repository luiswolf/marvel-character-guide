//
//  DataWrapperProtocol.swift
//  marvel-character-guide
//
//  Created by Luis Emilio Dias Wolf on 21/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation

protocol DataWrapperProtocol {
    
    associatedtype DataContainerType
    
    var code: Int? { get set }
    var status: String? { get set }
    var data: DataContainerType? { get set }
    
}
