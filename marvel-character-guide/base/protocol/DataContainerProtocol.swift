//
//  DataContainerProtocol.swift
//  marvel-character-guide
//
//  Created by Luis Emilio Dias Wolf on 21/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation

protocol DataContainerProtocol {
    
    associatedtype DataType
    
    var offset: Int? { get set }
    var limit: Int? { get set }
    var total: Int? { get set }
    var count: Int? { get set }
    var results: [DataType]? { get set }
    
}
