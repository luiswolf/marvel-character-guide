//
//  StoryboardProtocol.swift
//  marvel-character-guide
//
//  Created by Luis Emilio Dias Wolf on 21/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import UIKit

protocol StoryboardProtocol {
    
    static func instantiate(controllerIn _storyboard: StoryboardNamingProtocol) -> Self
    
    static func instantiate(navigationControllerIn _storyboard: StoryboardNamingProtocol) -> UINavigationController
    
}

extension StoryboardProtocol {
    
    static func instantiate(controllerIn _storyboard: StoryboardNamingProtocol) -> Self {
        let className = String(describing: self)
        let storyboard = UIStoryboard(name: _storyboard.getRawValue(), bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: className) as! Self
    }
    
    static func instantiate(navigationControllerIn _storyboard: StoryboardNamingProtocol) -> UINavigationController {
        let className = String(describing: self)
        let storyboard = UIStoryboard(name: _storyboard.getRawValue(), bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: className)
        return UINavigationController(rootViewController: vc)
    }
    
}
