//
//  ViewModelDelegate.swift
//  marvel-character-guide
//
//  Created by Luis Emilio Dias Wolf on 23/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation

protocol ViewModelDelegate: class {
    func didGetData()
    func didGetError(withMessage message: String)
}
