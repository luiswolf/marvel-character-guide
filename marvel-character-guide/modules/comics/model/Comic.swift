//
//  Comic.swift
//  marvel-character-guide
//
//  Created by Luis Emilio Dias Wolf on 22/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation

struct Comic: Decodable {
    
    var id: Int?
    var title: String?
    var thumbnail: Image?
    
}
