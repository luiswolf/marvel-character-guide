//
//  Character.swift
//  marvel-character-guide
//
//  Created by Luis Emilio Dias Wolf on 21/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation

struct Character: Decodable {
    
    var id: Int?
    var name: String?
    var description: String?
    var thumbnail: Image?
    
}
