//
//  CharactersDetailTableViewController.swift
//  marvel-character-guide
//
//  Created by Luis Emilio Dias Wolf on 22/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import UIKit

class CharactersDetailTableViewController: TableViewController, StoryboardProtocol {

    var character: Character!
    
    fileprivate let imageHelper = ImageHelper.sharedInstance
    fileprivate enum Cell: String {
        case comic, loading
    }
    fileprivate enum Section: Int, CaseIterable {
        case detail, comics
    }
    fileprivate enum CollectionViewSection: Int, CaseIterable {
        case comics, pagination
    }
    fileprivate lazy var viewModel: CharactersDetailViewModel = {
        let vm = CharactersDetailViewModel()
        vm.delegate = self
        return vm
    }()
    
    @IBOutlet fileprivate weak var thumbnailImageView: UIImageView!
    @IBOutlet fileprivate weak var nameLabel: UILabel!
    @IBOutlet fileprivate weak var descriptionLabel: UILabel!
    @IBOutlet fileprivate weak var comicsCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        backgroundColor = .darkGray
        tableView.estimatedRowHeight = 72
        tableView.rowHeight = UITableView.automaticDimension
        
        nameLabel.text = character.name
        nameLabel.font = UIFontMetrics(forTextStyle: .title3).scaledFont(for: UIFont.display.withSize(17.0))
        
        descriptionLabel.text = character.description
        descriptionLabel.font = UIFontMetrics(forTextStyle: .body).scaledFont(for: UIFont.regular)
        
        thumbnailImageView.image = UIImage(named: AppConstant.Image.logo)
        thumbnailImageView.contentMode = .scaleAspectFill
        thumbnailImageView.layer.cornerRadius = 64.0
        thumbnailImageView.layer.masksToBounds = true
        if let imageUrl = character.thumbnail?.getFullPath(), !imageUrl.contains("image_not_available") {
            imageHelper.getImage(withPath: imageUrl) { [weak self] image in
                guard let self = self, let image = image else { return }
                self.thumbnailImageView.image = image
            }
        }
        
        comicsCollectionView.delegate = self
        comicsCollectionView.dataSource = self
        if let flowLayout = comicsCollectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.minimumInteritemSpacing = 0
            flowLayout.minimumLineSpacing = 8
            comicsCollectionView?.collectionViewLayout = flowLayout
        }
        
        startLoading()
        getData()
    }
    
}

// MARK: - UITableView Delegate/DataSource
extension CharactersDetailTableViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let _section = Section(rawValue: section) else { return 0 }
        switch _section {
            case .detail:
                guard let description = character.description, description.isEmpty else {
                    return super.tableView(tableView, numberOfRowsInSection: section)
                }
                return 1
            case .comics:
                guard let comics = viewModel.comics, comics.isEmpty else {
                    return super.tableView(tableView, numberOfRowsInSection: section)
                }
                return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let imageView = UIImageView(image: UIImage(named: AppConstant.Image.backgroundCell))
        imageView.alpha = 0.05
        cell.backgroundView = imageView
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let _section = Section(rawValue: section), _section == .comics, let numberOfItems = viewModel.numberOfItems else {
            return super.tableView(tableView, titleForHeaderInSection: section)
        }
        return String(format: CharactersConstant.Title.comics, arguments: [numberOfItems])
    }
    
}

// MARK: - UICollectionView Delegate/DataSource
extension CharactersDetailTableViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        guard viewModel.comics != nil else { return 0 }
        return CollectionViewSection.allCases.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let _section = CollectionViewSection(rawValue: section) else { return 0 }
        switch _section {
        case .comics:
            guard let comics = viewModel.comics else { return 0 }
            return comics.count
        case .pagination:
            return viewModel.getNumberOfItemsForPagination()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let _section = CollectionViewSection(rawValue: indexPath.section) else {
            return UICollectionViewCell()
        }
        switch _section {
        case .comics:
            guard let comics = viewModel.comics else {
                return UICollectionViewCell()
            }
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Cell.comic.rawValue, for: indexPath) as? ComicCollectionViewCell else {
                return UICollectionViewCell()
            }
            cell.configure(forComic: comics[indexPath.item])
            return cell
        case .pagination:
            getData()
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Cell.loading.rawValue, for: indexPath) as? ComicLoadingCollectionViewCell else {
                return UICollectionViewCell()
            }
            return cell
        }
    }
    
}

// MARK: - Helper
extension CharactersDetailTableViewController {
    
    final fileprivate func getData() {
        guard let characterId = character.id else {
            errorAction(withMessage: AppConstant.Message.unknownError, hasAction: false)
            return
        }
        viewModel.list(comicsForCharacterId: characterId)
    }
    
    final fileprivate func successAction() {
        tableView.reloadData()
        comicsCollectionView.reloadData()
        stopLoading()
    }
    
    final fileprivate func errorAction(withMessage message: String, hasAction: Bool = true) {
        stopLoading()
        infoView.state = .error
        infoView.delegate = self
        infoView.message = message
        infoView.hasActionButton = hasAction
        showInfo()
    }
    
}

// MARK: - InfoViewDelegate
extension CharactersDetailTableViewController: InfoViewDelegate {
    
    final internal func didPressRetryButton() {
        hideInfo()
        startLoading()
        getData()
    }
    
}

// MARK: - ViewModelDelegate
extension CharactersDetailTableViewController: ViewModelDelegate {
    
    final internal func didGetData() {
        successAction()
    }
    
    final internal func didGetError(withMessage message: String) {
        errorAction(withMessage: message)
    }
    
}
