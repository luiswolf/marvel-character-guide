//
//  CharactersTableViewController.swift
//  marvel-character-guide
//
//  Created by Luis Emilio Dias Wolf on 22/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import UIKit

class CharactersTableViewController: TableViewController, StoryboardProtocol {

    fileprivate enum Cell: String {
        case character, loading
    }
    fileprivate enum Section: Int, CaseIterable {
        case characters, pagination
    }
    fileprivate lazy var viewModel: CharactersViewModel = {
        let vm = CharactersViewModel()
        vm.delegate = self
        return vm
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        backgroundColor = .darkGray
        tableView.estimatedRowHeight = 72
        tableView.rowHeight = UITableView.automaticDimension
        
        startLoading()
        viewModel.list()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? CharactersDetailTableViewController, let indexPath = tableView.indexPathForSelectedRow, let characters = viewModel.characters {
            vc.character = characters[indexPath.row]
        }
    }
    
}

// MARK: - UITableView Delegate/DataSource
extension CharactersTableViewController {
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 8.0
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        guard viewModel.characters != nil else { return 0 }
        return Section.allCases.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let _section = Section(rawValue: section) else { return 0 }
        switch _section {
            case .characters:
                guard let characters = viewModel.characters else { return 0 }
                return characters.count
            case .pagination:
                return viewModel.getNumberOfRowsForPagination()
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let _section = Section(rawValue: indexPath.section) else { return UITableViewCell() }
        switch _section {
            case .characters:
                guard let characters = viewModel.characters else { return UITableViewCell() }
                guard let cell = tableView.dequeueReusableCell(withIdentifier: Cell.character.rawValue, for: indexPath) as? CharacterTableViewCell else {
                    return UITableViewCell()
                }
                cell.configure(forCharacter: characters[indexPath.row])
                return cell
            case .pagination:
                viewModel.list()
                let cell = tableView.dequeueReusableCell(withIdentifier: Cell.loading.rawValue, for: indexPath)
                cell.textLabel?.text = String(format: AppConstant.Message.loadingNextPage, arguments: [viewModel.currentPage + 1, viewModel.numberOfPages ?? 0])
                return cell
        }
    }
    
}

// MARK: - Helper
extension CharactersTableViewController {
    
    final fileprivate func successAction() {
        tableView.reloadData()
        stopLoading()
    }
    
    final fileprivate func errorAction(withMessage message: String) {
        stopLoading()
        infoView.state = .error
        infoView.delegate = self
        infoView.message = message
        infoView.hasActionButton = true
        showInfo()
    }
    
    final fileprivate func noDataAction() {
        stopLoading()
        infoView.state = .noData
        infoView.message = CharactersConstant.Message.noData
        infoView.hasActionButton = false
        infoView.delegate = self
        showInfo()
    }
    
}

// MARK: - InfoViewDelegate
extension CharactersTableViewController: InfoViewDelegate {
    
    final internal func didPressRetryButton() {
        hideInfo()
        startLoading()
        viewModel.list()
    }
    
}


// MARK: - ViewModelDelegate
extension CharactersTableViewController: ViewModelDelegate {
    
    final internal func didGetData() {
        guard let characters = viewModel.characters else {
            errorAction(withMessage: CharactersConstant.Message.error)
            return
        }
        guard !characters.isEmpty else {
            noDataAction()
            return
        }
        successAction()
    }
    
    final internal func didGetError(withMessage message: String) {
        errorAction(withMessage: message)
    }
    
}
