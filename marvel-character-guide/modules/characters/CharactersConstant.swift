//
//  CharactersConstant.swift
//  marvel-character-guide
//
//  Created by Luis Emilio Dias Wolf on 21/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation

/// Constants for Characters module
enum CharactersConstant {
    
    /// List of module endpoints
    enum Endpoint {
        case list, detail(id: Int), comics(id: Int)
        
        private static let _list = AppConstant.Api.baseURL + "characters"
        private static let _detail = AppConstant.Api.baseURL + "characters/:id"
        private static let _comics = AppConstant.Api.baseURL + "characters/:id/comics"
        
        func getUrl(withOffset offset: Int? = nil) -> String? {
            var url: URLComponents?
            if case .list = self {
                url = URLComponents(string: Endpoint._list)
            }
            if case let .detail(id: id) = self {
                url = URLComponents(string: Endpoint._detail.replacingOccurrences(of: ":id", with: String(id)))
            }
            if case let .comics(id: id) = self {
                url = URLComponents(string: Endpoint._comics.replacingOccurrences(of: ":id", with: String(id)))
            }
            
            if let offset = offset {
                url?.queryItems = [URLQueryItem]()
                url?.queryItems?.append(URLQueryItem(name: "offset", value: String(offset)))
            }
            return url?.string
        }
    }
    
    /// List of module storyboards
    enum Storyboard: String, StoryboardNamingProtocol {
        internal func getRawValue() -> String {
            return self.rawValue
        }
        case main = "Characters"
    }
    
    /// List of module messages
    enum Message {
        static let error = "Não foi possível buscar personagens. Tente novamente mais tarde."
        static let noData = "Não há personagens para listar."
        static let noComics = "Não há quadrinhos para listar."
    }
    
    /// List of module titles
    enum Title {
        static let comics = "Histórias em quadrinhos: %d"
    }
    
}
