//
//  CharacterTableViewCell.swift
//  marvel-character-guide
//
//  Created by Luis Emilio Dias Wolf on 22/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import UIKit

class CharacterTableViewCell: UITableViewCell {

    fileprivate let imageHelper = ImageHelper.sharedInstance
    fileprivate let indicator = DisclosureIndicator()
    
    @IBOutlet fileprivate weak var thumbnailImageView: UIImageView!
    @IBOutlet fileprivate weak var nameLabel: UILabel!
    @IBOutlet fileprivate weak var descriptionLabel: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        accessoryView = indicator
        let imageView = UIImageView(image: UIImage(named: AppConstant.Image.backgroundCell))
        imageView.alpha = 0.05
        backgroundView = imageView
        
        nameLabel.font = UIFontMetrics(forTextStyle: .title3).scaledFont(for: UIFont.bold.withSize(17.0))
        descriptionLabel?.font = UIFontMetrics(forTextStyle: .caption1).scaledFont(for: UIFont.regular)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        if selected {
            backgroundColor = UIColor.darkGray.withAlphaComponent(0.7)
            indicator.color = UIColor.gray
        } else {
            backgroundColor = UIColor.gray
            indicator.color = UIColor.darkGray
        }
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        if highlighted {
            backgroundColor = UIColor.darkGray.withAlphaComponent(0.7)
            indicator.color = UIColor.gray
        } else {
            backgroundColor = UIColor.gray
            indicator.color = UIColor.darkGray
        }
    }
    
    func configure(forCharacter character: Character) {
        nameLabel.text = character.name
        descriptionLabel?.text = character.description
        
        thumbnailImageView.image = UIImage(named: AppConstant.Image.logo)
        thumbnailImageView.layer.cornerRadius = 32.0
        thumbnailImageView.layer.masksToBounds = true
        thumbnailImageView.contentMode = .scaleAspectFill
        if let imageUrl = character.thumbnail?.getFullPath(), !imageUrl.contains("image_not_available") {
            imageHelper.getImage(withPath: imageUrl) { [weak self] image in
                guard let self = self, let image = image else { return }
                self.thumbnailImageView.image = image
                self.thumbnailImageView.layer.cornerRadius = 32.0
                self.thumbnailImageView.layer.masksToBounds = true
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        thumbnailImageView.image = UIImage(named: AppConstant.Image.logo)
        thumbnailImageView.layer.cornerRadius = 32.0
        thumbnailImageView.layer.masksToBounds = true
        thumbnailImageView.contentMode = .scaleAspectFill
    }

}
