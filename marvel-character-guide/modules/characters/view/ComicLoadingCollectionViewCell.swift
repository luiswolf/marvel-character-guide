//
//  ComicLoadingCollectionViewCell.swift
//  marvel-character-guide
//
//  Created by Luis Emilio Dias Wolf on 23/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import UIKit

class ComicLoadingCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet fileprivate weak var activityIndicatorView: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        activityIndicatorView.startAnimating()
        activityIndicatorView.style = .whiteLarge
        activityIndicatorView.color = .darkGray
    }
    
}
