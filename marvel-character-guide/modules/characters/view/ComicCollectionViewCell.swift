//
//  ComicCollectionViewCell.swift
//  marvel-character-guide
//
//  Created by Luis Emilio Dias Wolf on 23/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import UIKit

class ComicCollectionViewCell: UICollectionViewCell {
    
    fileprivate let imageHelper = ImageHelper.sharedInstance
    
    @IBOutlet fileprivate weak var thumbnailImageView: UIImageView!
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        thumbnailImageView.image = UIImage(named: AppConstant.Image.logo)
        thumbnailImageView.contentMode = .scaleAspectFit
    }
    
}

// MARK: - Helper
extension ComicCollectionViewCell {
    
    func configure(forComic comic: Comic) {
        titleLabel.font = UIFontMetrics(forTextStyle: .subheadline).scaledFont(for: UIFont.display.withSize(16.0))
        titleLabel.text = comic.title
        
        thumbnailImageView.image = UIImage(named: AppConstant.Image.logo)
        thumbnailImageView.contentMode = .scaleAspectFit
        if let imageUrl = comic.thumbnail?.getFullPath() {
            imageHelper.getImage(withPath: imageUrl) { [weak self] image in
                guard let self = self, let image = image else { return }
                self.thumbnailImageView.image = image
            }
        }
    }
    
}
