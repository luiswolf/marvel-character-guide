//
//  CharactersService.swift
//  marvel-character-guide
//
//  Created by Luis Emilio Dias Wolf on 22/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation

class CharactersService: NSObject {
    
    fileprivate enum Params {
        static let id = ":id"
    }
    fileprivate lazy var requestHelper: RequestHelper = {
        return RequestHelper()
    }()
    
    func list(withOffset offset: Int = 0, andReturnTo callback: @escaping (_ : () throws -> DataContainer<Character>?) -> Void) {
        
        guard let url = CharactersConstant.Endpoint.list.getUrl(withOffset: offset) else {
            callback({ throw RequestError.urlError })
            return
        }
        
        requestHelper.getObject(ofType: Character.self, fromUrl: url) { data in
            do {
                let obj = try data()
                callback({ return obj.data })
            } catch let error {
                callback({ throw error })
            }
        }
        
    }
    
    func get(characterWithId id: Int, andReturnTo callback: @escaping (_ : () throws -> DataContainer<Character>?) -> Void) {
        
        guard let url = CharactersConstant.Endpoint.detail(id: id).getUrl() else {
            callback({ throw RequestError.urlError })
            return
        }
        
        requestHelper.getObject(ofType: Character.self, fromUrl: url) { data in
            do {
                let obj = try data()
                callback({ return obj.data })
            } catch let error {
                callback({ throw error })
            }
        }
        
    }
    
    func comics(forCharacterWithId id: Int, withOffset offset: Int = 0, andReturnTo callback: @escaping (_ : () throws -> DataContainer<Comic>?) -> Void) {
        
        guard let url = CharactersConstant.Endpoint.comics(id: id).getUrl(withOffset: offset) else {
            callback({ throw RequestError.urlError })
            return
        }
        
        requestHelper.getObject(ofType: Comic.self, fromUrl: url) { data in
            do {
                let obj = try data()
                callback({ return obj.data })
            } catch let error {
                callback({ throw error })
            }
        }
        
    }
    
}
