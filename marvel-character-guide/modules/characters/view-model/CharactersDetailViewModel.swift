//
//  CharactersDetailViewModel.swift
//  marvel-character-guide
//
//  Created by Luis Emilio Dias Wolf on 23/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation

class CharactersDetailViewModel: NSObject {
    
    weak var delegate: ViewModelDelegate?
    
    fileprivate(set) var comics: [Comic]?
    fileprivate(set) var numberOfPages: Int?
    fileprivate(set) var numberOfItems: Int?
    fileprivate(set) var currentPage: Int = 0
    fileprivate var offset: Int = 0
    fileprivate var limit: Int?
    fileprivate var isLoading: Bool = false
    fileprivate var canLoadNextPage: Bool = true
    
    fileprivate let service = CharactersService()
    
    final func getNumberOfItemsForPagination() -> Int {
        if canLoadNextPage { return 1 }
        return 0
    }
    
    final func list(comicsForCharacterId id: Int) {
        
        guard !isLoading else { return }
        guard canLoadNextPage else { return }
        
        isLoading = true
        service.comics(forCharacterWithId: id, withOffset: offset) { [weak self] (response: () throws -> DataContainer<Comic>?) in
            guard let self = self else { return }
            
            self.isLoading = false
            do {
                let container = try response()
                
                self.controlPagination(container: container)
                if let results = container?.results {
                    if self.comics == nil {
                        self.comics = [Comic]()
                    }
                    self.comics?.append(contentsOf: results)
                }
                self.delegate?.didGetData()
            } catch {
                self.delegate?.didGetError(withMessage: AppConstant.Message.networkError)
            }
        }
        
    }
    
}

// MARK: - Helper
extension CharactersDetailViewModel {
    
    fileprivate func controlPagination(container: DataContainer<Comic>?) {
        guard let container = container else { return }
        
        let numberOfPages: Int = {
            guard let total = container.total, let limit = container.limit else { return 1 }
            return Int((Float(total) / Float(limit)).rounded(.up))
        }()
        let currentPage: Int = {
            guard let offset = container.offset, let limit = container.limit else { return 1 }
            return (offset / limit) + 1
        }()
        
        self.numberOfItems = container.total
        self.isLoading = false
        self.offset += container.limit ?? 0
        self.canLoadNextPage = (currentPage + 1) <= numberOfPages
        self.limit = container.limit
        self.numberOfPages = numberOfPages
        self.currentPage = currentPage
    }
    
}
