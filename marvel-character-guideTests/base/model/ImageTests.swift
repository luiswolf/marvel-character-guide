//
//  ImageTests.swift
//  marvel-character-guideTests
//
//  Created by Luis Emilio Dias Wolf on 24/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import XCTest

@testable import marvel_character_guide

class ImageTests: XCTestCase {

    var image: Image!
    override func setUp() {
        
    }

    override func tearDown() {
        image = nil
    }

    func testGetFullPathReturnsUrl() {
        image = Image(path: "http://i.annihil.us/u/prod/marvel/i/mg/d/03/58dd080719806", ext: "jpg")
        XCTAssertEqual(image.getFullPath(), "http://i.annihil.us/u/prod/marvel/i/mg/d/03/58dd080719806.jpg")
    }
    
    func testGetFullPathReturnsNil() {
        image = Image(path: "http://i.annihil.us/u/prod/marvel/i/mg/d/03/58dd080719806", ext: nil)
        XCTAssertNil(image.getFullPath())
    }
}
