//
//  ImageHelperTests.swift
//  marvel-character-guideTests
//
//  Created by Luis Emilio Dias Wolf on 24/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import XCTest

@testable import marvel_character_guide

class ImageHelperTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        CacheHelper.sharedInstance.removeImage(forKey: "abcd")
        CacheHelper.sharedInstance.removeImage(forKey: "http://i.annihil.us/u/prod/marvel/i/mg/c/10/58dd01dbc6e51.jpg")
    }

    func testImageNotFound() {
        let expect = expectation(description: "Não localizar imagem e retornar nil")

        ImageHelper.sharedInstance.getImage(withPath: "abc") { image in
            XCTAssertNil(image)
            expect.fulfill()
        }
        waitForExpectations(timeout: 5) { error in
            if let error = error {
                XCTFail("Timeout com erro: \(error)")
            }
        }
        
    }
    
    func testImageStoredInCacheReturned() {
        guard let image = UIImage(named: "empty") else { return }
        let path = "abcd"
        CacheHelper.sharedInstance.storeImage(image, forKey: path)
        
        let expect = expectation(description: "Imagem registrada no cache e retornada pelo método getImage()")
        
        ImageHelper.sharedInstance.getImage(withPath: "abcd") { image in
            XCTAssertTrue(image != nil)
            expect.fulfill()
        }
        waitForExpectations(timeout: 5) { error in
            if let error = error {
                XCTFail("Timeout com erro: \(error)")
            }
        }
    }
    
    func testImageDownloaded() {
        let imageUrl = "http://i.annihil.us/u/prod/marvel/i/mg/c/10/58dd01dbc6e51.jpg"
        
        let expect = expectation(description: "Baixar arquivo de imagem e salvar no cache")
        ImageHelper.sharedInstance.getImage(withPath: imageUrl) { image in
            XCTAssertTrue(image != nil)
            let cacheImage = CacheHelper.sharedInstance.getImage(forKey: imageUrl)
            XCTAssertTrue(cacheImage != nil)
            expect.fulfill()
        }
        waitForExpectations(timeout: 5) { error in
            if let error = error {
                XCTFail("Timeout com erro: \(error)")
            }
        }
    }
    
    func testNotImageDownloaded() {
        let imageUrl = "http://i.annihil.us/u/prod/marvel/i/mg/c/10/58dd01dbc6e51jpg"
        
        let expect = expectation(description: "Não baixar arquivo pois não é uma url de imagem válida")
        ImageHelper.sharedInstance.getImage(withPath: imageUrl) { image in
            XCTAssertNil(image)
            let cacheImage = CacheHelper.sharedInstance.getImage(forKey: imageUrl)
            XCTAssertNil(cacheImage)
            expect.fulfill()
        }
        waitForExpectations(timeout: 5) { error in
            if let error = error {
                XCTFail("Timeout com erro: \(error)")
            }
        }
    }


}
