//
//  ComicTests.swift
//  marvel-character-guideTests
//
//  Created by Luis Emilio Dias Wolf on 24/12/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import XCTest

@testable import marvel_character_guide

class ComicTests: XCTestCase {

    private let rightJson = Data("""
    {
        "id": 22506,
        "title": "Avengers: The Initiative (2007) #19",
        "thumbnail": {
            "path": "http://i.annihil.us/u/prod/marvel/i/mg/d/03/58dd080719806",
            "extension": "jpg"
        }
    }
    """.utf8)
    
    private let wrongJson = Data("""
    {
        "id": 22506,
        "title": { "Avengers: The Initiative (2007) #19" },
        "thumbnail": {
            "path": "http://i.annihil.us/u/prod/marvel/i/mg/d/03/58dd080719806",
            "extension": "jpg"
        }
    }
    """.utf8)
    
    func testSuccessfulDecode() {
        let comic = try? JSONDecoder().decode(Comic.self, from: rightJson)
        XCTAssertEqual(comic?.id, 22506, "id incorreto")
        XCTAssertEqual(comic?.title, "Avengers: The Initiative (2007) #19", "title incorreto")
        XCTAssertEqual(comic?.thumbnail?.ext, "jpg", "extensão incorreta")
        XCTAssertEqual(comic?.thumbnail?.path, "http://i.annihil.us/u/prod/marvel/i/mg/d/03/58dd080719806", "path incorreto")
    }

    func testFailDecode() {
        do {
            let _ = try JSONDecoder().decode(Comic.self, from: wrongJson)
            XCTFail("Teste deve falhar quando json estiver correto.")
        } catch let error {
            XCTAssertTrue(error is DecodingError)
        }
    }
    
}
