# Guia de personagens Marvel

Aplicativo desenvolvido em teste para vaga de desenvolvedor iOS entre os dias 21/12/2019 e 23/12/2019.

> **OBJETIVO**
>
> Criar um aplicativo que utilize a API pública da Marvel: https://developer.marvel.com/
> 
> Esse aplicativo deverá listar os personagens em uma tela com rolagem infinita e apresentar os detalhes do personagem em uma tela auxiliar com suas devidas participações em Comics (informação disponível na API).
> 
> Lembre-se que você será avaliado não apenas em relação ao código, mas também em questões como: UI, UX e organização do projeto.
>
> Premissas:
>
> * Repositório Público (Github / Bitbucket / Gitlab);
>
> * README (Com setup do projeto caso necessário);
>
> * Código Nativo (Swift / Java / Kotlin);
>
> * Arquitetura MVP ou MVVM;
>
> * Testes Unitários (colocar % de cobertura no README);
>
> Observações:
>
> * Sinta-se a vontade para utilizar código open source;
>
> * Não é necessário apresentar todos os detalhes disponíveis na API.
>
> Tempo de Prova: 72h
>

Iniciei o projeto através do template Single View Application porém removi a interface principal e fiz o setup manual no AppDelegate.

Escolhi como versão mínima a versão do iOS 11 pois fiz o uso de Dynamic types em todos textos do aplicativo com objetivo de deixar a aplicação acessível.


## Requisitos

* iOS 11.0+
* Xcode 10.3+
* Swift 5.0+

## Dependências

Para gerenciamento de dependências, utilizei o CocoaPods com a biblioteca abaixo (em versão estável):

* [Alamofire] - Biblioteca que realiza chamadas de serviços para APIs Rest.

[Alamofire]: <https://github.com/Alamofire/Alamofire>

## Testes unitários

Como não trabalho com testes unitários atualmente, acabei deixando para fazer no final mas não tive tempo para aplicar em todas classes do projeto.
Adicionei em apenas algumas classes e o percentual de cobertura de código ficou em **44,8%**.

<img src="screenshots/code-coverage.png" width="700">

## Apresentação da aplicação

### Splash

Tela de apresentação do aplicativo.

<img src="screenshots/splash.png" width="300">

### Personagens

Tela principal do aplicativo, onde são exibidos os personagens do universo Marvel em uma lista contendo a imagem, nome e descrição resumida do personagem (quando existir).

Cada requisição possui o tamanho de 20 registros (padrão da API) e, ao chegar no final dos registros visíveis, é realizada uma nova consulta para buscar novos registros. Esta consulta é representada com uma indição no final da lista informando ao usuário sobre a consulta da próxima página e o total de páginas disponíveis.

<img src="screenshots/characters-01.png" width="300"><img src="screenshots/characters-02.png" width="300">

*** 

Caso ocorra um erro na chamada de serviço ou a mesma não retorne registros, são exibidas as telas abaixo, de erro e sem dados, respectivamente.

<img src="screenshots/characters-04.png" width="300"><img src="screenshots/characters-03.png" width="300">

***

### Detalhes do personagem

Ao selecionar um personagem na lista de personagens, o usuário é direcionado à tela de detalhes do personagem.

Nela såo exibidos a imagem, nome e descrição completa do personagem (se existir).

Também såo exibidos todas as histórias em quadrinhos (quando existir) que o personagem é encontrado.

Da mesma forma que os personagens são carregados, os quadrinhos também utilizam paginação, exibindo no final dos itens um indicador de carregamento quando houver mais itens a serem carregados.

<img src="screenshots/detail-01.png" width="300"><img src="screenshots/detail-02.png" width="300"><img src="screenshots/detail-03.png" width="300">


Caso ocorra um erro na chamada de serviço de quadrinhos, é exibida a tela abaixo.

<img src="screenshots/detail-04.png" width="300">
